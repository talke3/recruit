<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['name','email'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }  
    
    public static function userslist($cid,$uid){
        $userslist = DB::table('candidates')->where('id',$cid)->pluck('user_id');
        return self::find($userslist)->all(); 
        
    }

}

